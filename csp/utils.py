# -*- coding: utf-8 -*-
#
# This file is part of Toolforge Content Security Policy violation reports
#
# Copyright (C) 2018 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Utilities."""

import hashlib
import json
import math
import os
import pwd
import urllib.parse

import redis


class Cache(object):
    """Simple Redis cache interface."""

    def __init__(self, enabled=True):
        """Create object."""
        self.enabled = enabled
        self.conn = redis.Redis(
            host="tools-redis.svc.eqiad.wmflabs", decode_responses=True,
        )
        u = pwd.getpwuid(os.getuid())
        self.prefix = hashlib.sha1(
            "{}.{}".format(u.pw_name, u.pw_dir).encode("utf-8")
        ).hexdigest()

    def key(self, val):
        """Generate a key."""
        return "%s%s" % (self.prefix, val)

    def load(self, key):
        """Fetch from cache."""
        if self.enabled:
            try:
                return json.loads(self.conn.get(self.key(key)) or "")
            except ValueError:
                return None
        else:
            return None

    def save(self, key, data, expiry=3600):
        """Store in cache."""
        if self.enabled:
            real_key = self.key(key)
            self.conn.setex(real_key, expiry, json.dumps(data))


def pagination(total, current, size, around=4):
    """Compute pagination data."""
    pc = math.ceil(total / size)
    left = max(0, current - around)
    right = min(max(0, pc), current + around)
    return {
        "count": pc,
        "left": left,
        "right": right,
    }


def qs_merge(orig, **kwargs):
    """Update a collection of parameters and generate a query string."""
    ctx = orig.copy()
    ctx.update(kwargs)
    return urllib.parse.urlencode(ctx, doseq=True)

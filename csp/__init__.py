# -*- coding: utf-8 -*-
#
# This file is part of Toolforge Content Security Policy violation reports
#
# Copyright (C) 2018 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Toolforge Content Security Policy violation reports."""

import functools
import logging
import logging.config
import os
import re
import time
import urllib.parse

import elasticsearch
import elasticsearch.exceptions
import yaml

import csp.utils as utils


# Load configuration from YAML file
__dir__ = os.path.dirname(__file__)
CONFIG = yaml.safe_load(open(os.path.join(__dir__, "..", "config.yaml")))

logging.config.dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "%(asctime)s %(name)s %(levelname)s: %(message)s",
                "datefmt": "%Y-%m-%dT%H:%M:%SZ",
            },
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stderr",
                "formatter": "default",
            },
        },
        "root": {"level": CONFIG["LOG_LEVEL"], "handlers": ["wsgi"]},
    }
)
logger = logging.getLogger("csp-report")

es = elasticsearch.Elasticsearch(
    CONFIG["elasticsearch"]["servers"], **CONFIG["elasticsearch"]["options"]
)

RE_ALLOWED_DOMAINS = re.compile(
    "^.*({})$".format(
        "|".join(re.escape(d) for d in CONFIG["allowed_domains"])
    )
)

CACHE = utils.Cache()


def store_report(req):
    """Store CSP report."""
    if req.content_length is not None and req.content_length > 8192:
        # Limit abuse by limiting POST size to 8k
        logger.debug("Excessive content size. Report ignored.")
        return "", 413

    resp = "", 204
    payload = req.get_json(force=True, silent=True)
    if payload is None:
        logger.debug("Report payload either missing or invalid json.")
        return resp

    if "csp-report" not in payload:
        logger.debug('Report payload missing "csp-report" key.')
        return resp

    report = payload["csp-report"]
    if "line-number" in report and report["line-number"] == 1:
        # Ignore reports of errors on line 1. This is a common signature for
        # CSP errors triggered by client controlled code (e.g. browser plugins
        # that inject CSS/JS into all pages).
        logger.debug("Report for line-number=1 likely plugin noise.")
        return resp

    if not report["blocked-uri"].startswith("http"):
        # Ignore non-HTTP reports. Live capture has seen all kinds of weird
        # protocols like "gsa://", "opera://", "safari-extension://", etc
        logger.debug(
            'Report for non-HTTP blocked URI "%s"', report["blocked-uri"]
        )
        return resp

    url = urllib.parse.urlparse(report["document-uri"])
    if url.hostname == "tools.wmflabs.org":
        try:
            tool = url.path.split("/")[1]
        except IndexError:
            tool = "admin"
    else:
        try:
            tool = url.hostname.split(".")[0]
        except IndexError:
            tool = "www"

    blocked_url = urllib.parse.urlparse(report["blocked-uri"])
    if blocked_url and blocked_url.hostname:
        blocked_host = blocked_url.hostname.lower()
    else:
        blocked_host = "unknown"

    # Apparently some User Agents don't follow the spec and ignore wildcards
    # in the directive. Filter out false positives that come from that.
    if RE_ALLOWED_DOMAINS.match(blocked_host):
        logger.debug("False report for %s: %s", tool, report["blocked-uri"])
        return resp

    try:
        es.index(
            index=time.strftime(
                CONFIG["elasticsearch"]["index"], time.gmtime()
            ),
            doc_type="csp",
            body={
                "@timestamp": time.strftime(
                    "%Y-%m-%dT%H:%M:%SZ", time.gmtime()
                ),
                "tool": tool,
                "document-uri": report["document-uri"],
                "blocked-uri": report["blocked-uri"],
                "blocked-site": blocked_host,
            },
        )
        logger.debug("%s %s", tool, blocked_host)
    except elasticsearch.exceptions.ElasticsearchException:
        logger.exception("Failure storing report.")
    return resp


def top_things(field, size):
    """Get the top N results for a field."""
    res = es.search(
        index=CONFIG["elasticsearch"]["search_index"],
        doc_type="csp",
        body={
            "size": 0,
            "aggregations": {
                "top": {"terms": {"field": field, "size": size}},
            },
        },
    )
    return {
        b["key"]: b["doc_count"] for b in res["aggregations"]["top"]["buckets"]
    }


def cached(key, expiry=3600):
    """Cache decorated function return value."""

    def real_cached(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            cache_key = "{}:{}{}".format(
                key,
                ";".join(args),
                ";".join("{}={}".format(k, v) for k, v in kwargs.items()),
            )
            cached = kwargs.get("cached", True)
            r = CACHE.load(cache_key) if cached else None
            if r is None:
                r = f(*args, **kwargs)
                CACHE.save(key, r, expiry=expiry)
            return r

        return wrapper

    return real_cached


@cached("top_tools", 300)
def top_tools(size=10, cached=True):
    """Get a dict of top tools with CSP reports."""
    return top_things("tool.keyword", size)


@cached("top_blocked", 300)
def top_blocked(size=10, cached=True):
    """Get a dict of top external sites with CSP reports."""
    return top_things("blocked-site.keyword", size)


def search(form):
    """Search CSP reports."""
    filters = {"bool": {}}
    if form["q"]:
        filters["bool"]["must"] = {
            "query_string": {
                "query": form["q"],
                "default_operator": "AND",
                "lenient": True,
            },
        }
    if form["ft"] or form["fs"]:
        filters["bool"]["filter"] = []
        if form["ft"]:
            filters["bool"]["filter"].append(
                {"term": {"tool.keyword": form["ft"]}}
            )
        if form["fs"]:
            filters["bool"]["filter"].append(
                {"term": {"blocked-site.keyword": form["fs"]}}
            )

    query = {
        "from": form["p"] * form["i"],
        "size": form["i"],
        "sort": [{form["s"]: {"order": form["o"]}}],
        "query": filters,
        "aggregations": {
            "tool": {"terms": {"field": "tool.keyword", "size": 10}},
            "site": {"terms": {"field": "blocked-site.keyword", "size": 10}},
        },
    }
    res = es.search(
        index=CONFIG["elasticsearch"]["search_index"],
        doc_type="csp",
        body=query,
    )
    return res

Toolforge Content Security Policy violation reports
===================================================

Collect Content-Security-Policy violation reports, store them in
Elasticsearch, and allow user to see collected information.

Configuration
-------------
Place a `config.yaml` file in the same directory as app.py:

```
---
DEBUG: False
JSON_AS_ASCII: False
LOG_LEVEL: WARNING
LOGGER_HANDLER_POLICY: always
SECRET_KEY: "my really long secret for encrypting session data"
SESSION_COOKIE_HTTPONLY: True
SESSION_COOKIE_NAME: csp-report.sess
SESSION_COOKIE_PATH: /path/to/this/tool
SESSION_COOKIE_SECURE: True
TEMPLATES_AUTO_RELOAD: False

elasticsearch:
  servers:
    - tools-elastic-01.tools.eqiad.wmflabs
    - tools-elastic-02.tools.eqiad.wmflabs
    - tools-elastic-03.tools.eqiad.wmflabs
  options:
    port: 80
    http_auth:
      - elasticsearch username
      - elasticsearch password
    sniff_on_start: false
    sniff_on_connection_fail: false
  index: 'csp-%Y.%m.%d'
  search_index: 'csp-*'

allowed_domains:
  - mediawiki.org
  - wikibooks.org
  - wikidata.org
  - wikimedia.org
  - wikimediafoundation.org
  - wikinews.org
  - wikipedia.org
  - wikiquote.org
  - wikisource.org
  - wikiversity.org
  - wikivoyage.org
  - wiktionary.org
  - wmflabs.org
```

License
-------
[GPL-3.0-or-later](https://www.gnu.org/copyleft/gpl.html "GNU GPL 3.0 or later")

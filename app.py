# -*- coding: utf-8 -*-
#
# This file is part of Toolforge Content Security Policy violation reports
#
# Copyright (C) 2018 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Handle Content Security Policy violation reports."""
import logging

import elasticsearch.exceptions
import flask
import toolforge
import werkzeug.contrib.fixers

import csp
import csp.utils


# Create the Flask application
app = flask.Flask(__name__)
app.config.update(csp.CONFIG)
# Add the ProxyFix middleware which reads X-Forwarded-* headers
app.wsgi_app = werkzeug.contrib.fixers.ProxyFix(app.wsgi_app)
# Always use TLS
app.before_request(toolforge.redirect_to_https)


@app.route("/")
def index():
    """Application landing page."""
    cached = "purge" not in flask.request.args
    return flask.render_template(
        "index.html",
        tools=csp.top_tools(size=10, cached=cached),
        blocked=csp.top_blocked(size=10, cached=cached),
    )


@app.route("/collect", methods=["POST"])
def collect():
    """Collect a report."""
    return csp.store_report(flask.request)


@app.route("/tools")
def tools():
    """List tools with reports."""
    cached = "purge" not in flask.request.args
    return flask.render_template(
        "tools.html", tools=csp.top_tools(size=1000, cached=cached)
    )


@app.route("/sites")
def sites():
    """List sites with reports."""
    cached = "purge" not in flask.request.args
    return flask.render_template(
        "sites.html", sites=csp.top_blocked(size=1000, cached=cached)
    )


@app.route("/search")
def search():
    """Search reports."""
    form = {
        "q": flask.request.args.get("q", None),
        "i": int(flask.request.args.get("i", 20)),
        "p": int(flask.request.args.get("p", 0)),
        "s": flask.request.args.get("s", "@timestamp"),
        "o": flask.request.args.get("o", "desc"),
        "ft": flask.request.args.get("ft", None),
        "fs": flask.request.args.get("fs", None),
    }
    if (form["p"] + 1) * form["i"] > 10000:
        flask.flash(
            "Maximum results exceeded. Please refine your query.", "warning"
        )
        return flask.redirect(flask.url_for("search"))

    try:
        r = csp.search(form)
    except elasticsearch.exceptions.ElasticsearchException:
        logging.exception("Search failed")
        flask.flash("An error occurred while searching.", "danger")
        return flask.redirect(flask.url_for("search"))

    hits = {
        "all": r["hits"]["total"]["value"],
        "first": 1 + (form["p"] * form["i"]),
    }
    hits["last"] = min(hits["first"] + form["i"] - 1, hits["all"])
    pagination = csp.utils.pagination(
        r["hits"]["total"]["value"], form["p"], form["i"]
    )
    return flask.render_template(
        "search.html", form=form, results=r, hits=hits, pagination=pagination
    )


@app.route("/test")
def test_csp():
    """Test reporting."""
    resp = flask.make_response(flask.render_template("test.html"))
    resp.headers["Content-Security-Policy-Report-Only"] = (
        "default-src 'self' data: 'unsafe-inline' "
        "*.mediawiki.org "
        "*.wikibooks.org "
        "*.wikidata.org "
        "*.wikimedia.org "
        "*.wikinews.org "
        "*.wikipedia.org "
        "*.wikiquote.org "
        "*.wikisource.org "
        "*.wikiversity.org "
        "*.wikivoyage.org "
        "*.wiktionary.org "
        "*.wmflabs.org "
        "wikimediafoundation.org "
        "; report-uri https://tools.wmflabs.org/csp-report/collect;"
    )
    return resp


@app.context_processor
def utility_processor():
    """Add utilities to jinja context."""

    def qs_merge(**kwargs):
        return csp.utils.qs_merge(
            flask.request.args.to_dict(flat=False), **kwargs
        )

    return dict(qs_merge=qs_merge)


@app.errorhandler(404)
def error_404(e):
    """Handle 404 errors."""
    return flask.render_template("404.html"), 404


@app.errorhandler(500)
def error_500(e):
    """Handle 500 errors."""
    return flask.render_template("500.html", error=e), 500


if __name__ == "__main__":
    app.run()
